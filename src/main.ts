import "./style.css";

import { GUI } from "dat.gui";
import {
  WebGL1Renderer,
  PerspectiveCamera,
  Scene,
  WebGLRenderer,
  AmbientLight,
  PointLight,
  Mesh,
  MeshStandardMaterial,
  BoxGeometry,
  PointLightHelper,
  GridHelper,
} from "three";

import { OrbitControls } from "three/examples/jsm/Addons.js";

const viewAngle: number = 75;
const aspectRatio: number = window.innerWidth / window.innerHeight;
const viewClippingMin: number = 0.1;
const viewClippingMax: number = 1000;

const scene: Scene = new Scene();
const camera: PerspectiveCamera = new PerspectiveCamera(
  viewAngle,
  aspectRatio,
  viewClippingMin,
  viewClippingMax
);

// setup opengl renderer
const renderer: WebGLRenderer = new WebGL1Renderer({
  canvas: document?.querySelector("#bg")!,
});

renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);
camera.position.setZ(30);
camera.position.setY(20);
camera.position.setX(-10);

// create cube
const geometry: BoxGeometry = new BoxGeometry(10, 10, 10);
const material: MeshStandardMaterial = new MeshStandardMaterial({
  color: 0xff0000,
});
const cube: Mesh = new Mesh(geometry, material);
scene.add(cube);

// add light source
const pointLight: PointLight = new PointLight(0xffffff);
pointLight.position.set(20, 10, 20);
pointLight.intensity = 5000;

// add second light source
const ambientLight: AmbientLight = new AmbientLight(0xffffff);
ambientLight.intensity = 0.2;

// add both lights to the scene
scene.add(pointLight, ambientLight);

// add light souce indicator and wire grid
const lightHelper: PointLightHelper = new PointLightHelper(pointLight);
const gridHelper: GridHelper = new GridHelper(200, 50);
scene.add(lightHelper);
scene.add(gridHelper);

const acceleration: number = 0.5;
// add simple keycodes
document.onkeydown = function (e) {
  if (e.key == "w" || e.key == "W") {
    cube.position.z -= 1 * acceleration;
  }
  if (e.key == "a" || e.key == "A") {
    cube.position.x -= 1 * acceleration;
  }
  if (e.key == "s" || e.key == "S") {
    cube.position.z += 1 * acceleration;
  }
  if (e.key == "d" || e.key == "D") {
    cube.position.x += 1 * acceleration;
  }
  if (e.key == " ") {
    cube.position.y += 1 * acceleration;
  }
};

// add orbit controls
new OrbitControls(camera, renderer.domElement);

// add dat gui
const gui = new GUI();
const cubeFolder = gui.addFolder("Cube");
cubeFolder.add(cube.rotation, "x", 0, Math.PI * 4);
cubeFolder.add(cube.rotation, "y", 0, Math.PI * 4);
cubeFolder.add(cube.rotation, "z", 0, Math.PI * 4);
cubeFolder.open();

// update loop
function update() {
  requestAnimationFrame(update);

  if (window) renderer.render(scene, camera);
}
update();
